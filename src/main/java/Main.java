import AerialVehicles.AerialVehicleType;
import AerialVehicles.F15;
import AerialVehicles.F15Missions;
import AerialVehicles.MissionHandler;
import Entities.Coordinates;
import Missions.AttackMission;
import Missions.IntelligenceMission;
import Missions.Mission;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello");
        F15 a = new F15(new Coordinates(100.0, 200.5));
        a.hoursSinceFix = 200;
        Mission mission = new IntelligenceMission(new Coordinates(300.4, 403.4), "Jhon Jhons", a, "House");
        mission.begin();
        mission.finish();
    }
}