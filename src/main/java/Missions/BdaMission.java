package Missions;

import AerialVehicles.AerialVehicle;
import AerialVehicles.AvailableMission;
import Entities.Coordinates;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class BdaMission extends Mission {
    private String objective;

    public BdaMission(Coordinates missionObjective, String pilotName, AerialVehicle executingAerialVehicle, String objective) {
        super(missionObjective, pilotName, executingAerialVehicle);
        this.setObjective(objective);
    }

    public String getObjective() {
        return this.objective;
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }

    @Override
    public void executeMission() throws AerialVehicleNotCompatibleException {

        ArrayList<AvailableMission> missions =
                new ArrayList<AvailableMission>(this.getExecutingAerialVehicle().availableMissions().stream().filter(availableMission
                        -> availableMission instanceof AerialVehicles.BdaMission).collect(Collectors.toList()));
        if (missions.isEmpty()) {
            throw new AerialVehicleNotCompatibleException("Aerial Vehicle not compatible with this mission");
        } else {
            System.out.println(this.getPilotName() + ": " + this.getExecutingAerialVehicle().getClass().getSimpleName()
                    + " taking pictures of " + this.getObjective() + " with: " + missions.get(0));
        }
    }
}
