package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;

public abstract class Mission{
    private Coordinates missionObjective;
    private String pilotName;
    private AerialVehicle executingAerialVehicle;

    public Mission(Coordinates missionObjective, String pilotName, AerialVehicle executingAerialVehicle) {
        this.setMissionObjective(missionObjective);
        this.setPilotName(pilotName);
        this.setExecutingAerialVehicle(executingAerialVehicle);
    }

    public Coordinates getMissionObjective() {
        return this.missionObjective;
    }

    public void setMissionObjective(Coordinates missionObjective) {
        this.missionObjective = missionObjective;
    }

    public String getPilotName() {
        return this.pilotName;
    }

    public void setPilotName(String pilotName) {
        this.pilotName = pilotName;
    }

    public AerialVehicle getExecutingAerialVehicle() {
        return this.executingAerialVehicle;
    }

    public void setExecutingAerialVehicle(AerialVehicle executingAerialVehicle) {
        this.executingAerialVehicle = executingAerialVehicle;
    }

    public void begin() {
        System.out.println("Beginning Mission!");
        this.executingAerialVehicle.flyTo(missionObjective);
    }

    public void cancel() {
        System.out.println("Abort Mission!");
        this.executingAerialVehicle.land(this.executingAerialVehicle.getHomeBase());
    }

    public void finish() {
        try {
            this.executeMission();
            this.executingAerialVehicle.land(this.executingAerialVehicle.getHomeBase());
            System.out.println("Mission Has Finished!");
        } catch (AerialVehicleNotCompatibleException e) {
            System.out.println("The current Aerial Vehicle is not compatible with this mission");
        }
    }

    public void executeMission() throws AerialVehicleNotCompatibleException {

    }
}
