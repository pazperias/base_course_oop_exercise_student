package Missions;

import AerialVehicles.AerialVehicle;
import AerialVehicles.AvailableMission;
import Entities.Coordinates;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class AttackMission extends Mission {
    private String target;

    public AttackMission(Coordinates missionObjective, String pilotName, AerialVehicle executingAerialVehicle, String target) {
        super(missionObjective, pilotName, executingAerialVehicle);
        this.setTarget(target);
    }

    public String getTarget() {
        return this.target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    @Override
    public void executeMission() throws AerialVehicleNotCompatibleException {

        ArrayList<AvailableMission> missions =
                new ArrayList<AvailableMission>(this.getExecutingAerialVehicle().availableMissions().stream().filter(availableMission
                        -> availableMission instanceof AerialVehicles.AttackMission).collect(Collectors.toList()));

        if (missions.isEmpty()) {
            throw new AerialVehicleNotCompatibleException("Aerial Vehicle not compatible with this mission");
        } else {
            System.out.println(missions);
            System.out.println(missions.get(0));
            String text = this.getPilotName() + ": " + this.getExecutingAerialVehicle().getClass().getSimpleName()
                    + " Attacking " + this.getTarget() + " with: " + missions.get(0);
            System.out.println(text);
        }
    }
}
