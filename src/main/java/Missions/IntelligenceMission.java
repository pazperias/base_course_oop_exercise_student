package Missions;

import AerialVehicles.AerialVehicle;
import AerialVehicles.AvailableMission;
import Entities.Coordinates;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class IntelligenceMission extends Mission {
    private String region;

    public IntelligenceMission(Coordinates missionObjective, String pilotName, AerialVehicle executingAerialVehicle, String region) {
        super(missionObjective, pilotName, executingAerialVehicle);
        this.setRegion(region);
    }

    public String getRegion() {
        return this.region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    @Override
    public void executeMission() throws AerialVehicleNotCompatibleException {

        ArrayList<AvailableMission> missions =
                new ArrayList<AvailableMission>(this.getExecutingAerialVehicle().availableMissions().stream().filter(availableMission
                        -> availableMission instanceof AerialVehicles.IntelligenceMission).collect(Collectors.toList()));

        if (missions.isEmpty()) {
            throw new AerialVehicleNotCompatibleException("Aerial Vehicle not compatible with this mission");
        } else {
            System.out.println(this.getPilotName() + ": " + this.getExecutingAerialVehicle().getClass().getSimpleName()
                    + " Collecting Data in " + this.getRegion() + " with: sensor type: " + missions.get(0));
        }
    }
}
