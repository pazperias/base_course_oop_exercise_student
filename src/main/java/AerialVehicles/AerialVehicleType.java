package AerialVehicles;

public enum AerialVehicleType {
    F15,
    F16,
    ZIK,
    SHOVAL,
    EITAN,
    KOCHAV
}
