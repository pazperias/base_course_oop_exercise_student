package AerialVehicles;

import Entities.Coordinates;

public class F15 extends AerialVehicle {
    public F15(Coordinates homeBase) {
        super(homeBase);
        this.MAX_HOURS_FROM_FIX = 250;
        this.loadMissions(AerialVehicleType.F15);
    }
}
