package AerialVehicles;

public enum CameraType {
    REGULAR,
    THERMAL,
    NIGHT_VISION
}
