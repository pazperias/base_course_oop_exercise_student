package AerialVehicles;

import Entities.Coordinates;

public abstract class Uav extends AerialVehicle {
    public Uav(Coordinates homeBase) {
        super(homeBase);
    }

    public void hoverOverLocation(Coordinates destination) {
        if (this.flightStatus() != FlightStatus.READY) {
            System.out.println("Uav Vehicle isn't ready to fly");
        } else {
            this.changeFlightStatus(FlightStatus.IN_THE_AIR);
            System.out.println("Hovering over: " + destination);
        }
    }
}
