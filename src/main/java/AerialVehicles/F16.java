package AerialVehicles;

import Entities.Coordinates;

public class F16 extends AerialVehicle {
    public F16(Coordinates homeBase) {
        super(homeBase);
        this.MAX_HOURS_FROM_FIX = 250;
        this.loadMissions(AerialVehicleType.F16);
    }
}
