package AerialVehicles;

import Entities.Coordinates;

public class Zik extends Uav {
    public Zik(Coordinates homeBase) {
        super(homeBase);
        this.MAX_HOURS_FROM_FIX = 100;
        this.loadMissions(AerialVehicleType.ZIK);
    }
}