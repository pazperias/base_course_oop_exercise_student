package AerialVehicles;

public class AttackMission extends AvailableMission{
    private int missleAmount;
    private MissleType missleType;

    public AttackMission(String[] missleInfo) {
        this.missleAmount = Integer.parseInt(missleInfo[1]);
        this.missleType = MissleType.valueOf(missleInfo[0]);
    }

    @Override
    public String toString() {
        return this.missleType.toString() + "X" + this.missleAmount;
    }
}
