package AerialVehicles;

public class IntelligenceMission extends AvailableMission {
    private SensorType sensorType;

    public IntelligenceMission(String[] sensorType) {
        this.sensorType = SensorType.valueOf(sensorType[0]);
    }

    @Override
    public String toString() {
        return this.sensorType.toString();
    }
}

