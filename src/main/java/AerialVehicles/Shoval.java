package AerialVehicles;


import Entities.Coordinates;

public class Shoval extends Uav {
    public Shoval(Coordinates homeBase) {
        super(homeBase);
        this.MAX_HOURS_FROM_FIX = 150;
        this.loadMissions(AerialVehicleType.SHOVAL);
    }
}

