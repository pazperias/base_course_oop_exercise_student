package AerialVehicles;

public enum FlightStatus {
    READY,
    NOT_READY,
    IN_THE_AIR
}
