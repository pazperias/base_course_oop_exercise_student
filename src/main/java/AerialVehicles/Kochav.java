package AerialVehicles;

import Entities.Coordinates;

public class Kochav extends Uav {
    public Kochav(Coordinates homeBase) {
        super(homeBase);
        this.MAX_HOURS_FROM_FIX = 100;
        this.loadMissions(AerialVehicleType.KOCHAV);
    }
}
