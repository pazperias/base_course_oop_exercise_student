package AerialVehicles;

import Entities.Coordinates;

import java.util.ArrayList;
import java.util.List;

public abstract class AerialVehicle {
    public int hoursSinceFix;
    private FlightStatus flightStatus;
    private Coordinates homeBase;
    private List<AvailableMission> availableMissions;
    private MissionHandler missionHandler;
    public static int MAX_HOURS_FROM_FIX = 100;

    public AerialVehicle(Coordinates homeBase) {
        this.hoursSinceFix = 0;
        this.setHomeBase(homeBase);
        this.flightStatus = FlightStatus.READY;
        this.missionHandler = new MissionHandler();
    }

    public void loadEquipment() {

    }

    public void loadMissions(AerialVehicleType aerialVehicleType) {
        this.setAvailableMissions(this.missionHandler.availableMissions(aerialVehicleType, new String[][]{{"PYTHON", "2"},{"INFRA_RED"}, {"REGULAR"}}));
    }

    public List<AvailableMission> availableMissions() {
        return this.availableMissions;
    }

    private void setAvailableMissions(List<AvailableMission> availableMissions) {
        this.availableMissions = new ArrayList<AvailableMission>(availableMissions);
    }

    public Coordinates getHomeBase() {
        return this.homeBase;
    }

    public void setHomeBase(Coordinates homeBase) {
        this.homeBase = homeBase;
    }

    public int hoursSinceFix() {
        return this.hoursSinceFix;
    }

    public void resetHoursSinceFix() {
        this.hoursSinceFix = 0;
    }

    public FlightStatus flightStatus() {
        return this.flightStatus;
    }

    public void changeFlightStatus(FlightStatus flightStatus) {
        this.flightStatus = flightStatus;
    }

    public void flyTo(Coordinates destination) {
        if(this.flightStatus() != FlightStatus.READY) {
            System.out.println("Aerial Vehicle isn't ready to fly");
        } else {
            System.out.println("Flying to: " + destination);
            this.changeFlightStatus(FlightStatus.IN_THE_AIR);
        }
    }

    public void land(Coordinates destination) {
        if(this.flightStatus() != FlightStatus.IN_THE_AIR) {
            System.out.println("Aerial Vehicle isn't in the air");
        } else {
            System.out.println("Landing at: " + destination);
            this.check();
        }
    }

    public void check() {
        System.out.println("Checking Aerial Vehicle");
        if(this.hoursSinceFix() >= MAX_HOURS_FROM_FIX) {
            this.changeFlightStatus(FlightStatus.NOT_READY);
            this.repair();
        } else {
            this.changeFlightStatus(FlightStatus.READY);
        }
    }

    public void repair() {
        System.out.println("Repairing Aerial vehicle");
        this.resetHoursSinceFix();
        this.changeFlightStatus(FlightStatus.READY);
    }
}
