package AerialVehicles;

import Entities.Coordinates;

public class Eitan extends Uav {
    public Eitan(Coordinates homeBase) {
        super(homeBase);
        this.MAX_HOURS_FROM_FIX = 150;
        this.loadMissions(AerialVehicleType.EITAN);
    }
}
