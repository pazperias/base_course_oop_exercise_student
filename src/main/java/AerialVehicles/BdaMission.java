package AerialVehicles;

public class BdaMission extends AvailableMission{
    private CameraType cameraType;

    public BdaMission(String[] cameraType) {
        this.cameraType = CameraType.valueOf(cameraType[0]);
    }

    @Override
    public String toString() {
        return this.cameraType.toString();
    }
}
