package AerialVehicles;

import java.util.ArrayList;
import java.util.List;


public class MissionHandler {

    // Gets the type of the vehicle and returns its available missions
    public ArrayList<AvailableMission> availableMissions(AerialVehicleType aerialVehicleType, String[][] equipments) {
        ArrayList<AvailableMission> availableMissions = new ArrayList<AvailableMission>();

        switch (aerialVehicleType) {
            case F15 -> availableMissions = getMissionsByEnum(F15Missions.class, equipments);
            case F16 -> availableMissions = getMissionsByEnum(F16Missions.class, equipments);
            case ZIK -> availableMissions = getMissionsByEnum(ZikMissions.class, equipments);
            case EITAN -> availableMissions = getMissionsByEnum(EitanMissions.class, equipments);
            case KOCHAV -> availableMissions = getMissionsByEnum(KochavMissions.class, equipments);
        }
        return availableMissions;
    }

    private ArrayList<AvailableMission> getMissionsByEnum(Class<?> enumClass, String[][] equipments) {
        ArrayList<AvailableMission> availableMissions = new ArrayList<AvailableMission>();


        // Add each mission for the specific Aerial Vehicle into a list
        for(Object mission : enumClass.getEnumConstants()) {
            switch (mission.toString()) {
                case "ATTACK":
                    availableMissions.add(new AttackMission(equipments[0]));
                    break;
                case "INTELLIGENCE":
                    availableMissions.add(new IntelligenceMission(equipments[1]));
                    break;
                case "BDA":
                    availableMissions.add(new BdaMission(equipments[2]));
                    break;
            }

        }

        return availableMissions;
    }
}
